import { Platform } from 'react-native';

import cGeChapterTitleMap from './assets/titles/ge-titles';
import cExChapterTitleMap from './assets/titles/ex-titles';
import cLevChapterTitleMap from './assets/titles/lev-titles';
import cNuChapterTitleMap from './assets/titles/nu-titles';
import cDtChapterTitleMap from './assets/titles/dt-titles';
import cJosChapterTitleMap from './assets/titles/jos-titles';
import cJdgChapterTitleMap from './assets/titles/jdg-titles';
import cRuChapterTitleMap from './assets/titles/ru-titles';
import c1SaChapterTitleMap from './assets/titles/1sa-titles';
import c2SaChapterTitleMap from './assets/titles/2sa-titles';
import c1KiChapterTitleMap from './assets/titles/1ki-titles';
import c2KiChapterTitleMap from './assets/titles/2ki-titles';
import c1ChChapterTitleMap from './assets/titles/1ch-titles';
import c2ChChapterTitleMap from './assets/titles/2ch-titles'; 
import cEzrChapterTitleMap from './assets/titles/ezr-titles';
import cNeChapterTitleMap from './assets/titles/ne-titles';
import cEstChapterTitleMap from './assets/titles/est-titles';
import cJobChapterTitleMap from './assets/titles/job-titles';
import cPsChapterTitleMap from './assets/titles/ps-titles';
import cPrChapterTitleMap from './assets/titles/pr-titles';
import cEccChapterTitleMap from './assets/titles/ecc-titles';
import cSsChapterTitleMap from './assets/titles/ss-titles';
import cIsaChapterTitleMap from './assets/titles/isa-titles';
import cJerChapterTitleMap from './assets/titles/jer-titles';
import cLaChapterTitleMap from './assets/titles/la-titles';
import cEzeChapterTitleMap from './assets/titles/eze-titles';
import cDaChapterTitleMap from './assets/titles/da-titles';
import cHosChapterTitleMap from './assets/titles/hos-titles';
import cJoelChapterTitleMap from './assets/titles/joel-titles';
import cAmChapterTitleMap from './assets/titles/am-titles';
import cObChapterTitleMap from './assets/titles/ob-titles';
import cJnhChapterTitleMap from './assets/titles/jnh-titles';
import cMicChapterTitleMap from './assets/titles/mic-titles';
import cNaChapterTitleMap from './assets/titles/na-titles';
import cHabChapterTitleMap from './assets/titles/hab-titles';
import cZepChapterTitleMap from './assets/titles/zep-titles';
import cHagChapterTitleMap from './assets/titles/hag-titles';
import cZecChapterTitleMap from './assets/titles/zec-titles';
import cMalChapterTitleMap from './assets/titles/mal-titles';

import cMtChapterTitleMap from './assets/titles/mt-titles';
import cMkChapterTitleMap from './assets/titles/mk-titles';
import cLkChapterTitleMap from './assets/titles/lk-titles';
import cJnChapterTitleMap from './assets/titles/jn-titles';
import cAcChapterTitleMap from './assets/titles/ac-titles';
import cRoChapterTitleMap from './assets/titles/ro-titles';
import c1CoChapterTitleMap from './assets/titles/1co-titles';
import c2CoChapterTitleMap from './assets/titles/2co-titles';
import cGalChapterTitleMap from './assets/titles/gal-titles';
import cEphChapterTitleMap from './assets/titles/eph-titles';
import cPhpChapterTitleMap from './assets/titles/php-titles';
import cColChapterTitleMap from './assets/titles/col-titles';
import c1ThChapterTitleMap from './assets/titles/1th-titles';
import c2ThChapterTitleMap from './assets/titles/2th-titles';
import c1TiChapterTitleMap from './assets/titles/1ti-titles';
import c2TiChapterTitleMap from './assets/titles/2ti-titles';
import cTitusChapterTitleMap from './assets/titles/titus-titles';
import cPhmChapterTitleMap from './assets/titles/phm-titles';
import cHebChapterTitleMap from './assets/titles/heb-titles';
import cJasChapterTitleMap from './assets/titles/jas-titles';
import c1PeChapterTitleMap from './assets/titles/1pe-titles';
import c2PeChapterTitleMap from './assets/titles/2pe-titles';
import c1JnChapterTitleMap from './assets/titles/1jn-titles';
import c2JnChapterTitleMap from './assets/titles/2jn-titles';
import c3JnChapterTitleMap from './assets/titles/3jn-titles';
import cJudeChapterTitleMap from './assets/titles/jude-titles';
import cRevChapterTitleMap from './assets/titles/rev-titles';

/* Old Testament Book List */
export const OldTestamentBooks = [
  "Ge", "Ex", "Lev", "Nu", "Dt", "Jos", "Jdg", "Ru", "1Sa", "2Sa",
  "1Ki", "2Ki", "1Ch", "2Ch", "Ezr", "Ne", "Est", "Job", "Ps", "Pr",
  "Ecc", "SS", "Isa", "Jer", "La", "Eze", "Da", "Hos", "Joel", "Am",
  "Ob", "Jnh", "Mic", "Na", "Hab", "Zep", "Hag", "Zec", "Mal"
];

/* New Testament Book List */
export const NewTestamentBooks = [
  "Mt", "Mk", "Lk", "Jn", "Ac", "Ro", "1Co", "2Co", "Gal", "Eph",
  "Php", "Col", "1Th", "2Th", "1Ti", "2Ti", "Titus", "Phm", "Heb", "Jas",
  "1Pe", "2Pe", "1Jn", "2Jn", "3Jn", "Jude", "Rev"
];

export const BibleBooksTitleMap = {
  "Ge": "Genesis", "Ex": "Exodus", "Lev": "Leviticus", "Nu": "Numbers", "Dt": "Deuteronomy",
  "Jos": "Joshua", "Jdg": "Judges", "Ru": "Ruth", "1Sa": "1 Samuel", "2Sa": "2 Samuel",
  "1Ki": "1 Kings", "2Ki": "2 Kings", "1Ch": "1 Chronicles", "2Ch": "2 Chronicles", "Ezr": "Ezra",
  "Ne": "Nehemiah", "Est": "Esther", "Job": "Job", "Ps": "Psalms", "Pr": "Proverbs",
  "Ecc": "Ecclesiastes", "SS": "Song of Solomon", "Isa": "Isaiah", "Jer": "Jeremiah", "La": "Lamentations",

  "Eze": "Ezekiel", "Da": "Daniel", "Hos": "Hosea", "Joel": "Joel", "Am": "Amos",
  "Ob": "Obadiah", "Jnh": "Jonah", "Mic": "Micah", "Na": "Nahum", "Hab": "Habakkuk",
  "Zep": "Zephaniah", "Hag": "Haggai", "Zec": "Zechariah", "Mal": "Malachi", 
  
  "Mt": "Matthew", "Mk": "Mark", "Lk": "Luke", "Jn": "John", "Ac": "Acts", 
  "Ro": "Romans", "1Co": "1 Corinthians", "2Co": "2 Corinthians", "Gal": "Galatians", "Eph": "Ephesians", 
  "Php": "Philippians", "Col": "Colossians", "1Th": "1 Thessalonians", "2Th": "2 Thessalonians", "1Ti": "1 Timothy",  
  "2Ti": "2 Timothy", "Titus": "Titus", "Phm": "Philemon", "Heb": "Hebrews", "Jas": "James", 
  "1Pe": "1 Peter", "2Pe": "2 Peter", "1Jn": "1 John", "2Jn": "2 John", "3Jn": "3 John", 
  
  "Jude": "Jude", "Rev": "Revelation"
};

/* Chapter Count - Old Testament */
const _GeCnt = 50, _ExCnt = 40, _LevCnt = 27, _NuCnt = 36, _DtCnt = 34;
const _JosCnt = 24, _JdgCnt = 21, _RuCnt = 4, _1SaCnt = 31, _2SaCnt = 24;
const _1KiCnt = 22, _2KiCnt = 25, _1ChCnt = 29, _2ChCnt = 36, _EzrCnt = 10;
const _NeCnt = 13, _EstCnt = 10, _JobCnt = 42, _PsCnt = 150, _PrCnt = 31;
const _EccCnt = 12, _SSCnt = 8, _IsaCnt = 66, _JerCnt = 52, _LaCnt = 5;
const _EzeCnt = 48, _DaCnt = 12, _HosCnt = 14, _JoelCnt = 3, _AmCnt = 9;
const _ObCnt = 1, _JnhCnt = 4, _MicCnt = 7, _NaCnt = 3, _HabCnt = 3;
const _ZepCnt = 3, _HagCnt = 2, _ZecCnt = 14, _MalCnt = 4;

/* Chapter Count - New Testament */
const _MtCnt = 28, _MkCnt = 16, _LkCnt = 24, _JnCnt = 21, _AcCnt = 28;
const _RoCnt = 16, _1CoCnt = 16, _2CoCnt = 13, _GalCnt = 6, _EphCnt = 6;
const _PhpCnt = 4, _ColCnt = 4, _1ThCnt = 5, _2ThCnt = 3, _1TiCnt = 6;
const _2TiCnt = 4, _TitusCnt = 3, _PhmCnt = 1, _HebCnt = 13, _JasCnt = 5;
const _1PeCnt = 5, _2PeCnt = 3, _1JnCnt = 5, _2JnCnt = 1, _3JnCnt = 1;
const _JudeCnt = 1, _RevCnt = 22;

/* Old Testament Chapter List */
export const cGeChapters = Array.from(new Array(_GeCnt), (x,i) => i+1);
export const cExChapters = Array.from(new Array(_ExCnt), (x,i) => i+1);
export const cLevChapters = Array.from(new Array(_LevCnt), (x,i) => i+1);
export const cNuChapters = Array.from(new Array(_NuCnt), (x,i) => i+1);
export const cDtChapters = Array.from(new Array(_DtCnt), (x,i) => i+1);

export const cJosChapters = Array.from(new Array(_JosCnt), (x,i) => i+1);
export const cJdgChapters = Array.from(new Array(_JdgCnt), (x,i) => i+1);
export const cRuChapters = Array.from(new Array(_RuCnt), (x,i) => i+1);
export const c1SaChapters = Array.from(new Array(_1SaCnt), (x,i) => i+1);
export const c2SaChapters = Array.from(new Array(_2SaCnt), (x,i) => i+1);

export const c1KiChapters = Array.from(new Array(_1KiCnt), (x,i) => i+1);
export const c2KiChapters = Array.from(new Array(_2KiCnt), (x,i) => i+1);
export const c1ChChapters = Array.from(new Array(_1ChCnt), (x,i) => i+1);
export const c2ChChapters = Array.from(new Array(_2ChCnt), (x,i) => i+1);
export const cEzrChapters = Array.from(new Array(_EzrCnt), (x,i) => i+1);

export const cNeChapters = Array.from(new Array(_NeCnt), (x,i) => i+1);
export const cEstChapters = Array.from(new Array(_EstCnt), (x,i) => i+1);
export const cJobChapters = Array.from(new Array(_JobCnt), (x,i) => i+1);
export const cPsChapters = Array.from(new Array(_PsCnt), (x,i) => i+1);
export const cPrChapters = Array.from(new Array(_PrCnt), (x,i) => i+1);

export const cEccChapters = Array.from(new Array(_EccCnt), (x,i) => i+1);
export const cSSChapters = Array.from(new Array(_SSCnt), (x,i) => i+1);
export const cIsaChapters = Array.from(new Array(_IsaCnt), (x,i) => i+1);
export const cJerChapters = Array.from(new Array(_JerCnt), (x,i) => i+1);
export const cLaChapters = Array.from(new Array(_LaCnt), (x,i) => i+1);

export const cEzeChapters = Array.from(new Array(_EzeCnt), (x,i) => i+1);
export const cDaChapters = Array.from(new Array(_DaCnt), (x,i) => i+1);
export const cHosChapters = Array.from(new Array(_HosCnt), (x,i) => i+1);
export const cJoelChapters = Array.from(new Array(_JoelCnt), (x,i) => i+1);
export const cAmChapters = Array.from(new Array(_AmCnt), (x,i) => i+1);

export const cObChapters = Array.from(new Array(_ObCnt), (x,i) => i+1);
export const cJnhChapters = Array.from(new Array(_JnhCnt), (x,i) => i+1);
export const cMicChapters = Array.from(new Array(_MicCnt), (x,i) => i+1);
export const cNaChapters = Array.from(new Array(_NaCnt), (x,i) => i+1);
export const cHabChapters = Array.from(new Array(_HabCnt), (x,i) => i+1);

export const cZepChapters = Array.from(new Array(_ZepCnt), (x,i) => i+1);
export const cHagChapters = Array.from(new Array(_HagCnt), (x,i) => i+1);
export const cZecChapters = Array.from(new Array(_ZecCnt), (x,i) => i+1);
export const cMalChapters = Array.from(new Array(_MalCnt), (x,i) => i+1);

/* New Testament Chapter List */
export const cMtChapters = Array.from(new Array(_MtCnt), (x,i) => i+1);
export const cMkChapters = Array.from(new Array(_MkCnt), (x,i) => i+1);
export const cLkChapters = Array.from(new Array(_LkCnt), (x,i) => i+1);
export const cJnChapters = Array.from(new Array(_JnCnt), (x,i) => i+1);
export const cAcChapters = Array.from(new Array(_AcCnt), (x,i) => i+1);

export const cRoChapters = Array.from(new Array(_RoCnt), (x,i) => i+1);
export const c1CoChapters = Array.from(new Array(_1CoCnt), (x,i) => i+1);
export const c2CoChapters = Array.from(new Array(_2CoCnt), (x,i) => i+1);
export const cGalChapters = Array.from(new Array(_GalCnt), (x,i) => i+1);
export const cEphChapters = Array.from(new Array(_EphCnt), (x,i) => i+1);

export const cPhpChapters = Array.from(new Array(_PhpCnt), (x,i) => i+1);
export const cColChapters = Array.from(new Array(_ColCnt), (x,i) => i+1);
export const c1ThChapters = Array.from(new Array(_1ThCnt), (x,i) => i+1);
export const c2ThChapters = Array.from(new Array(_2ThCnt), (x,i) => i+1);
export const c1TiChapters = Array.from(new Array(_1TiCnt), (x,i) => i+1);

export const c2TiChapters = Array.from(new Array(_2TiCnt), (x,i) => i+1);
export const cTitusChapters = Array.from(new Array(_TitusCnt), (x,i) => i+1);
export const cPhmChapters = Array.from(new Array(_PhmCnt), (x,i) => i+1);
export const cHebChapters = Array.from(new Array(_HebCnt), (x,i) => i+1);
export const cJasChapters = Array.from(new Array(_JasCnt), (x,i) => i+1);

export const c1PeChapters = Array.from(new Array(_1PeCnt), (x,i) => i+1);
export const c2PeChapters = Array.from(new Array(_2PeCnt), (x,i) => i+1);
export const c1JnChapters = Array.from(new Array(_1JnCnt), (x,i) => i+1);
export const c2JnChapters = Array.from(new Array(_2JnCnt), (x,i) => i+1);
export const c3JnChapters = Array.from(new Array(_3JnCnt), (x,i) => i+1);

export const cJudeChapters = Array.from(new Array(_JudeCnt), (x,i) => i+1);
export const cRevChapters = Array.from(new Array(_RevCnt), (x,i) => i+1);

export const BibleBooksChapterMap = {
  "Ge": cGeChapters, "Ex": cExChapters, "Lev": cLevChapters, "Nu": cNuChapters, "Dt": cDtChapters,
  "Jos": cJosChapters, "Jdg": cJdgChapters, "Ru": cRuChapters, "1Sa": c1SaChapters, "2Sa": c2SaChapters,
  "1Ki": c1KiChapters, "2Ki": c2KiChapters, "1Ch": c1ChChapters, "2Ch": c2ChChapters, "Ezr": cEzrChapters,
  "Ne": cNeChapters, "Est": cEstChapters, "Job": cJobChapters, "Ps": cPsChapters, "Pr": cPrChapters,
  "Ecc": cEccChapters, "SS": cSSChapters, "Isa": cIsaChapters, "Jer": cJerChapters, "La": cLaChapters,

  "Eze": cEzeChapters, "Da": cDaChapters, "Hos": cHosChapters, "Joel": cJoelChapters, "Am": cAmChapters,
  "Ob": cObChapters, "Jnh": cJnhChapters, "Mic": cMicChapters, "Na": cNaChapters, "Hab": cHabChapters,
  "Zep": cZepChapters, "Hag": cHagChapters, "Zec": cZecChapters, "Mal": cMalChapters, 
  
  "Mt": cMtChapters, "Mk": cMkChapters, "Lk": cLkChapters, "Jn": cJnChapters, "Ac": cAcChapters, 
  "Ro": cRoChapters, "1Co": c1CoChapters, "2Co": c2CoChapters, "Gal": cGalChapters, "Eph": cEphChapters, 
  "Php": cPhpChapters, "Col": cColChapters, "1Th": c1ThChapters, "2Th": c2ThChapters, "1Ti": c1TiChapters, 
  "2Ti": c2TiChapters, "Titus": cTitusChapters, "Phm": cPhmChapters, "Heb": cHebChapters, "Jas": cJasChapters, 
  "1Pe": c1PeChapters, "2Pe": c2PeChapters, "1Jn": c1JnChapters, "2Jn": c2JnChapters,"3Jn": c3JnChapters,
  "Jude": cJudeChapters, "Rev": cRevChapters
};

/* Bible Chapter Title Map */
export const ChapterTitleMap = {
  "Ge": cGeChapterTitleMap,
  "Ex": cExChapterTitleMap,
  "Lev": cLevChapterTitleMap,
  "Nu": cNuChapterTitleMap,
  "Dt": cDtChapterTitleMap,
  "Jos": cJosChapterTitleMap,
  "Jdg": cJdgChapterTitleMap,
  "Ru": cRuChapterTitleMap,
  "1Sa": c1SaChapterTitleMap,
  "2Sa": c2SaChapterTitleMap,
  "1Ki": c1KiChapterTitleMap,
  "2Ki": c2KiChapterTitleMap,
  "1Ch": c1ChChapterTitleMap,
  "2Ch": c2ChChapterTitleMap,
  "Ezr": cEzrChapterTitleMap,
  "Ne": cNeChapterTitleMap,
  "Est": cEstChapterTitleMap,
  "Job": cJobChapterTitleMap,
  "Ps": cPsChapterTitleMap,
  "Pr": cPrChapterTitleMap,
  "Ecc": cEccChapterTitleMap,
  "SS": cSsChapterTitleMap,
  "Isa": cIsaChapterTitleMap,
  "Jer": cJerChapterTitleMap,
  "La": cLaChapterTitleMap,
  "Eze": cEzeChapterTitleMap,
  "Da": cDaChapterTitleMap,
  "Hos": cHosChapterTitleMap,
  "Joel": cJoelChapterTitleMap,
  "Am": cAmChapterTitleMap,
  "Ob": cObChapterTitleMap,
  "Jnh": cJnhChapterTitleMap,
  "Mic": cMicChapterTitleMap,
  "Na": cNaChapterTitleMap,
  "Hab": cHabChapterTitleMap,
  "Zep": cZepChapterTitleMap,
  "Hag": cHagChapterTitleMap,
  "Zec": cZecChapterTitleMap,
  "Mal": cMalChapterTitleMap,

  "Mt": cMtChapterTitleMap,
  "Mk": cMkChapterTitleMap,
  "Lk": cLkChapterTitleMap,
  "Jn": cJnChapterTitleMap,
  "Ac": cAcChapterTitleMap,
  "Ro": cRoChapterTitleMap,
  "1Co": c1CoChapterTitleMap,
  "2Co": c2CoChapterTitleMap,
  "Gal": cGalChapterTitleMap,
  "Eph": cEphChapterTitleMap,
  "Php": cPhpChapterTitleMap,
  "Col": cColChapterTitleMap,
  "1Th": c1ThChapterTitleMap,
  "2Th": c2ThChapterTitleMap,
  "1Ti": c1TiChapterTitleMap,
  "2Ti": c2TiChapterTitleMap,
  "Titus": cTitusChapterTitleMap,
  "Phm": cPhmChapterTitleMap,
  "Heb": cHebChapterTitleMap,
  "Jas": cJasChapterTitleMap,
  "1Pe": c1PeChapterTitleMap,
  "2Pe": c2PeChapterTitleMap,
  "1Jn": c1JnChapterTitleMap,
  "2Jn": c2JnChapterTitleMap,
  "3Jn": c3JnChapterTitleMap,
  "Jude": cJudeChapterTitleMap,
  "Rev": cRevChapterTitleMap
};

export function getInitialBookStats() { return {
  "Ge": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Ex": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Lev": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Nu": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Dt": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Jos": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Jdg": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Ru": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "1Sa": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "2Sa": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "1Ki": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "2Ki": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "1Ch": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "2Ch": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Ezr": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Ne": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Est": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Job": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Ps": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Pr": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Ecc": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "SS": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Isa": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Jer": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "La": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Eze": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Da": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Hos": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Joel": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Am": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Ob": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Jnh": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Mic": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Na": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Hab": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Zep": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Hag": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Zec": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Mal": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },

  "Mt": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Mk": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Lk": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Jn": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Ac": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Ro": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "1Co": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "2Co": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Gal": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Eph": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Php": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Col": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "1Th": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "2Th": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "1Ti": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "2Ti": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Titus": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Phm": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Heb": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Jas": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "1Pe": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "2Pe": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "1Jn": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "2Jn": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "3Jn": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Jude": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] },
  "Rev": { percentComplete: 0, chaptersRead: 0, chaptersReadList: [] }
}};


export function getInitialSummaryStats() {
  return {
    overallProgress: 0, chaptersRead: 0, 
    otProgress: 0, otChaptersRead: 0, 
    ntProgress: 0, ntChaptersRead: 0,
  };
}

export const TOTAL_CHAPTERS = 1189;
export const OLD_TESTAMENT_CHAPTERS = 929;
export const NEW_TESTAMENT_CHAPTERS = 260;

/* Redux Actions */
export const INIT_ASYNCSTORE = 'INIT_ASYNCSTORE';
export const INIT_ASYNCSTORE_SUCCEEDED = 'INIT_ASYNCSTORE_SUCCEEDED';
export const INIT_ASYNCSTORE_FAILED = 'INIT_ASYNCSTORE_FAILED';

export const MARK_CHAPTER_AS_READ = 'MARK_CHAPTER_AS_READ';
export const MARK_CHAPTER_AS_UNREAD = 'MARK_CHAPTER_AS_UNREAD';

export const CLEAR_COMPLETED_CHAPTERS = 'CLEAR_COMPLETED_CHAPTERS';

const ADMOB_IOS_BANNER_AD_ID = 'ca-app-pub-5257733333565838/3372683971';
const ADMOB_ANDROID_BANNER_AD_ID = 'ca-app-pub-5257733333565838/7719123096';

export const ADMOB_BANNER_ID = Platform.OS === 'ios' ? ADMOB_IOS_BANNER_AD_ID : ADMOB_ANDROID_BANNER_AD_ID;

