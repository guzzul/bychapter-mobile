export default cNaChapterTitleMap = {
  "1": "God's Wrath Against Nineveh",
  "2": "The Destruction of Nineveh",
  "3": "Woe to Nineveh",
};