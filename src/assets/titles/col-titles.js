export default cColChapterTitleMap = {
  "1": "The Preeminence of Christ",
  "2": "Spiritual Fullness in Christ",
  "3": "Put On the New Self",
  "4": "Christian Graces",
};