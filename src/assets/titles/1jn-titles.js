export default c1JnChapterTitleMap = {
  "1": "The Word of Life",
  "2": "Christ Our Advocate",
  "3": "The Command to Love",
  "4": "God Is Love",
  "5": "Overcoming the World",  
};