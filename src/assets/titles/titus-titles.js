export default cTitusChapterTitleMap = {
  "1": "Qualifications for Elders",
  "2": "Teach Sound Doctrine",
  "3": "Be Ready for Every Good Work",
};