export default cMicChapterTitleMap = {
  "1": "The Coming Destruction",
  "2": "Woe to the Oppressors",
  "3": "Rulers and Prophets Denounced",
  "4": "The Mountain of the Lord",
  "5": "The Ruler to Be Born in Bethlehem",
  "6": "God Pleads with Israel",
  "7": "Wait for the God of Salvation",  
};