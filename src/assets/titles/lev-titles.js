export default cLevChapterTitleMap = {
  "1": "Laws for Burnt Offerings",
  "2": "Laws for Grain Offerings",
  "3": "Laws for Peace Offerings",
  "4": "Laws for Sin Offerings",
  "5": "Laws for Guilt Offerings",
  "6": "The Priests and the Offerings",
  "7": "The Priests’ Share",
  "8": "The Ordination of Aaron and His Sons",
  "9": "The Priests Begin Their Ministry",
  "10": "The Death of Nadab and Abihu",
  "11": "Clean and Unclean Animals",
  "12": "Purification After Childbirth",
  "13": "Laws About Leprosy",
  "14": "Laws for Cleansing Lepers",
  "15": "Laws About Bodily Discharges",
  "16": "The Day of Atonement",
  "17": "Eating Blood Forbidden",
  "18": "Unlawful Sexual Relations",
  "19": "Various Laws",
  "20": "Punishments for Sin",
  "21": "Holiness and the Priests",
  "22": "Acceptable Offerings",
  "23": "The Appointed Festivals",
  "24": "Olive Oil and Bread Set Before the Lord",
  "25": "The Year of Jubilee",
  "26": "Reward for Obedience",
  "27": "Laws About Vows",
};