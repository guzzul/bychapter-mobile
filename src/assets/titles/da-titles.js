export default cDaChapterTitleMap = {
  "1": "Daniel’s Training in Babylon",
  "2": "Nebuchadnezzar’s Dream",
  "3": "The Image of Gold and the Blazing Furnace",
  "4": "Nebuchadnezzar’s Second Dream",
  "5": "The Handwriting on the Wall",
  "6": "Daniel and the Lions' Den",
  "7": "Daniel's Vision of the Four Beasts",
  "8": "Daniel's Vision of the Ram and the Goat",
  "9": "Daniel's Prayer for His People",
  "10": "Daniel's Terrifying Vision of a Man",
  "11": "The Kings of the South and the North",
  "12": "The Time of the End",  
};