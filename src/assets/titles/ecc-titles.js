export default cEccChapterTitleMap = {
  "1": "The Vanity of Life",
  "2": "Pleasures Are Meaningless",
  "3": "A Time for Everything",
  "4": "Evil Under the Sun",
  "5": "Fear God, Keep Your Vows",
  "6": "Wealth Is Not the Goal of Life",
  "7": "The Value of Practical Wisdom",
  "8": "Keep the King's Command",
  "9": "Death Comes to All",
  "10": "Wisdom and Folly",
  "11": "The Value of Diligence",
  "12": "Remember Your Creator in Your Youth",
};