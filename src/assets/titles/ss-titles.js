export default cSsChapterTitleMap = {
  "1": "Solomon and His Bride",
  "2": "The Bride Adores Her Beloved",
  "3": "The Bride's Dream",
  "4": "Solomon Admires His Bride's Beauty",
  "5": "The Bride Praises the Bridegroom",
  "6": "Solomon and His Bride",
  "7": "Expressions of Praise",
  "8": "Longing for Her Beloved",  
};