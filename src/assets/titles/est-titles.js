export default cEstChapterTitleMap = {
  "1": "Queen Vashti Deposed",
  "2": "Esther Chosen Queen",
  "3": "Haman Plots Against the Jews",
  "4": "Esther Agrees to Help the Jews",
  "5": "Esther Prepares a Banquet",
  "6": "Mordecai Honored",
  "7": "Haman Impaled",
  "8": "Esther Saves the Jews",
  "9": "The Jews Destroy Their Enemies",
  "10": "The Greatness of Mordecai",
};