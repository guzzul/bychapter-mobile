export default cMalChapterTitleMap = {
  "1": "The Lord's Love for Israel",
  "2": "The Lord Rebukes the Priests",
  "3": "The Coming Messenger",
  "4": "The Great Day of the Lord",
};