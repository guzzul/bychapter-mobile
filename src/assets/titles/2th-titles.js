export default c2ThChapterTitleMap = {
  "1": "The Judgment at Christ's Coming",
  "2": "The Man of Lawlessness",
  "3": "Warning Against Idleness",
};