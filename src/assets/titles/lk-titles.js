export default cLkChapterTitleMap = {
  "1": "Birth of Jesus Foretold",
  "2": "The Birth of Jesus Christ",
  "3": "John the Baptist Prepares the Way",
  "4": "The Temptation of Jesus",
  "5": "Jesus Calls His First Disciples",
  "6": "Jesus Is Lord of the Sabbath",
  "7": "Jesus Heals a Centurion's Servant",
  "8": "The Parable of the Sower",
  "9": "Jesus Feeds the Five Thousand",
  "10": "The Parable of the Good Samaritan",
  "11": "The Lord's Prayer",
  "12": "Warnings and Encouragements",
  "13": "The Parables of the Mustard Seed and the Yeast",
  "14": "The Parable of the Great Banquet",
  "15": "The Parable of the Prodigal Son",
  "16": "The Parable of the Unjust Steward",
  "17": "The Coming of the Kingdom",
  "18": "Jesus Blesses Little Children",
  "19": "The Triumphal Entry",
  "20": "The Authority of Jesus Challenged",
  "21": "The Destruction of the Temple and Signs of the End Times",
  "22": "The Last Supper",
  "23": "The Crucifixion",
  "24": "The Resurrection",
};