export default c1TiChapterTitleMap = {
  "1": "Warning Against False Teachers",
  "2": "Pray for All People",
  "3": "The Mystery of Godliness",
  "4": "A Good Servant of Christ Jesus",
  "5": "Instructions for the Church",
  "6": "Fight the Good Fight of Faith",
};