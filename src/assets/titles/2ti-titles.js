export default c2TiChapterTitleMap = {
  "1": "Guard the Deposit Entrusted to You",
  "2": "A Good Soldier of Christ Jesus",
  "3": "All Scripture Is Breathed Out by God",
  "4": "Preach the Word",
};