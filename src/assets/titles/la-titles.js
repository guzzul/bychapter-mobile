export default cLaChapterTitleMap = {
  "1": "Jerusalem in Affliction",
  "2": "God’s Anger with Jerusalem",
  "3": "The Prophet’s Anguish and Hope",
  "4": "The Degradation of Zion",
  "5": "Prayer for Restoration",  
};