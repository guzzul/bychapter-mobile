export default cEphChapterTitleMap = {
  "1": "Redemption in Christ",
  "2": "By Grace Through Faith",
  "3": "The Mystery of the Gospel Revealed",
  "4": "Unity in the Body of Christ",
  "5": "Walk in Love",
  "6": "The Whole Armor of God",
};