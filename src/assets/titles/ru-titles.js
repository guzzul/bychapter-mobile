export default cRuChapterTitleMap = {
  "1": "Naomi Loses Her Husband and Sons",
  "2": "Ruth Meets Boaz in the Grain Field",
  "3": "Ruth and Boaz at the Threshing Floor",
  "4": "Boaz Marries Ruth",  
};