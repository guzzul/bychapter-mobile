export default c1ThChapterTitleMap = {
  "1": "The Thessalonians' Faith and Example",
  "2": "Paul's Ministry to the Thessalonians",
  "3": "Timothy's Encouraging Report",
  "4": "A Life Pleasing to God",
  "5": "The Day of the Lord",
};