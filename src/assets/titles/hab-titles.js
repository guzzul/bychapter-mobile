export default cHabChapterTitleMap = {
  "1": "The Prophet Questions God’s Judgments",
  "2": "The Righteous Shall Live by His Faith",
  "3": "Habakkuk's Prayer",
};