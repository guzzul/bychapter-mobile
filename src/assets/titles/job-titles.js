export default cJobChapterTitleMap = {
  "1": "Job's Character and Wealth",
  "2": "Satan Attacks Job’s Health",
  "3": "Job Deplores His Birth",
  "4": "Eliphaz Speaks: The Innocent Prosper",
  "5": "Eliphaz: Job Is Chastened by God",
  "6": "Job Replies: My Complaint Is Just",
  "7": "Job Continues: My Life Has No Hope",
  "8": "Bildad Speaks: Job Should Repent",
  "9": "Job Replies: There Is No Arbiter",
  "10": "Job Continues: A Plea to God",
  "11": "Zophar Speaks: You Deserve Worse",
  "12": "Job Replies: The Lord Has Done This",
  "13": "Job Continues: Still I Will Hope in God",
  "14": "Job Continues: Death Comes Soon to All",
  "15": "Eliphaz Accuses: Job Does Not Fear God",
  "16": "Job Replies: Miserable Comforters Are You",
  "17": "Job Continues: Where Then Is My Hope?",
  "18": "Bildad Speaks: God Punishes the Wicked",
  "19": "Job Replies: My Redeemer Lives",
  "20": "Zophar Speaks: The Wicked Will Suffer",
  "21": "Job Replies: The Wicked Do Prosper",
  "22": "Eliphaz Speaks: Job's Wickedness Is Great",
  "23": "Job Replies: Where Is God?",
  "24": "Job Complains of Violence on the Earth",
  "25": "Bildad Speaks: Man Cannot Be Righteous",
  "26": "Job Replies: God's Majesty Is Unsearchable",
  "27": "Job Continues: I Will Maintain My Integrity",
  "28": "Job Continues: Where Is Wisdom?",
  "29": "Job's Summary Defense",
  "30": "Job’s Wealth Now Poverty",
  "31": "Job Defends His Righteousness",
  "32": "Elihu Rebukes Job's Three Friends",
  "33": "Elihu Rebukes Job",
  "34": "Elihu Asserts God's Justice",
  "35": "Elihu Condemns Self-Righteousness",
  "36": "Elihu Proclaims God’s Goodness",
  "37": "Elihu Proclaims God's Majesty",
  "38": "The Lord Answers Job",
  "39": "God Continues to Challenge Job",
  "40": "God’s Challenge to Job",
  "41": "God’s Power in the Leviathan",
  "42": "Job’s Repentance and Restoration",
};