export default cEzrChapterTitleMap = {
  "1": "End of the Babylonian Captivity",
  "2": "The Captives Who Returned to Jerusalem",
  "3": "Rebuilding the Altar and the Temple",
  "4": "Opposition to the Rebuilding",
  "5": "Tattenai’s Letter to Darius",
  "6": "The Decree of Darius",
  "7": "Ezra Sent to Teach the People",
  "8": "Genealogy of Those Who Returned with Ezra",
  "9": "Ezra’s Prayer About Intermarriage",
  "10": "The People Confess Their Sin",
};