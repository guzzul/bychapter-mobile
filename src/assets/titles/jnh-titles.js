export default cJnhChapterTitleMap = {
  "1": "Jonah Flees From the Lord",
  "2": "Jonah’s Prayer and God’s Answer",
  "3": "Jonah Goes to Nineveh",
  "4": "Jonah's Anger and the Lord's Compassion",
};