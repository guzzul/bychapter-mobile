export default cAmChapterTitleMap = {
  "1": "Judgment on the Nations",
  "2": "Judgment on Israel and Judah",
  "3": "Israel's Guilt and Punishment",
  "4": "Israel Has Not Returned to God",
  "5": "Seek the Lord and Live",
  "6": "Warnings to Zion and Samaria",
  "7": "Warning Visions",
  "8": "The Coming Day of Bitter Mourning",
  "9": "The Destruction of Israel",
};