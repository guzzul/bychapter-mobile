export default cPhpChapterTitleMap = {
  "1": "To Live Is Christ",
  "2": "Christ's Example of Humility",
  "3": "Righteousness Through Faith in Christ",
  "4": "Exhortation, Encouragement, and Prayer",
};