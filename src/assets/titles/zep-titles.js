export default cZepChapterTitleMap = {
  "1": "Judgment on the Whole Earth in the Day of the Lord",
  "2": "Judah and Jerusalem Judged Along With the Nations",
  "3": "Restoration of Israel’s Remnant",
};