export default cGalChapterTitleMap = {
  "1": "No Other Gospel",
  "2": "Defending the Gospel",
  "3": "Justification by Faith",
  "4": "Sons and Heirs Through Christ",
  "5": "Christ Has Set Us Free",
  "6": "Bear One Another's Burdens",
};