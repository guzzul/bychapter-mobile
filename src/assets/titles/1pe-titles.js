export default c1PeChapterTitleMap = {
  "1": "Called to Be Holy",
  "2": "A Living Stone and a Holy People",
  "3": "Suffering for Righteousness' Sake",
  "4": "Stewards of God's Grace",
  "5": "Shepherd the Flock of God",
};