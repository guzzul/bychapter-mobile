export default cJasChapterTitleMap = {
  "1": "Testing of Your Faith",
  "2": "Faith Without Works Is Dead",
  "3": "Taming the Tongue",
  "4": "Warning Against Worldliness",
  "5": "Patience in Suffering",
};