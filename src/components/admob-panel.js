import React from 'react';
import { View, StyleSheet } from 'react-native';

import { AdMobBanner } from 'expo';
import { ADMOB_BANNER_ID } from '../const';

export class AdmobPanel extends React.PureComponent {
  render() {
    return null;

    return(
      <View style={styles.container}>
        <AdMobBanner
            style={{ alignSelf: 'center' }}
            bannerSize="banner"
            adUnitID={ ADMOB_BANNER_ID }
            testDeviceID="EMULATOR"
            onDidFailToReceiveAdWithError={this.bannerError} 
          />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3399FF',
    height: 60
  }
})