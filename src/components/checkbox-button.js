import React from 'react';
import { TouchableOpacity, View, StyleSheet } from 'react-native';
import { Feather } from '@expo/vector-icons';

export class CheckBoxButton extends React.Component {
  constructor(props) {
    super(props);
    const { isChecked } = this.props;
    this.state = { isChecked: isChecked };
  }

  _onPress = () => {
    const { onValueChanged } = this.props;
    const isChecked = !this.state.isChecked; 
    this.setState({ isChecked: isChecked })

    if (onValueChanged) {
      onValueChanged(isChecked);
    }
  }

  render() {
    let iconName = '';
    if (this.state.isChecked) {
      iconName = 'check-square'
    } else {
      iconName = 'square'
    }

    return(
      <TouchableOpacity onPress={ this._onPress }>
        <Feather name={iconName} size={20}/>
      </TouchableOpacity>
    );
  }
}