import React from 'react';
import { 
  Text, 
  View,
  StyleSheet 
} from 'react-native';
import Collapsible from 'react-native-collapsible';

import { BibleBooksTitleMap } from '../const';
import { CheckBoxButton } from './checkbox-button';

export class UndoPanel extends React.PureComponent {
  constructor(props) {
    super(props);
    const { isRead } = this.props; 
    this.state = { isCollapsed: !isRead };
  }

  _onCheckBoxValueChanged = (checked) => {
    const { onMarkedUnread, book, chapter } = this.props;
    this.setState({ isCollapsed: !checked})

    if (onMarkedUnread && !checked ) {
      onMarkedUnread(book, chapter);
    }
  }

  render() {
    const { isCollapsed } = this.state;
    const { book, chapter, age } = this.props;

    const label = BibleBooksTitleMap[book] + ' ' + chapter;
    return(
      <Collapsible collapsed={isCollapsed}>
        <View style={ styles.listItem }>
          <CheckBoxButton isChecked onValueChanged={this._onCheckBoxValueChanged} />
          <Text style={ styles.text }>{ label }</Text>
          <View style={{flex: 1}}>
            <Text style={{ color: 'darkgrey', fontFamily: 'raleway-regular', fontSize: 12, textAlign: 'right'}}>{ age }</Text>
          </View>
        </View>
      </Collapsible>
    );
  }

}

const styles = StyleSheet.create({
  listItem: {
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    paddingLeft: 20,
    paddingRight: 20,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#bbb',
  },
  text: {
    paddingLeft: 10,
    textDecorationLine: 'line-through', 
    textDecorationStyle: 'solid',
    fontFamily: 'raleway-regular',
    fontSize: 16,
  }
});