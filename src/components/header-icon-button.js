import React from 'react';
import { TouchableOpacity, View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { Feather } from '@expo/vector-icons';

export class HeaderIconButton extends React.PureComponent {
  render() {
    const { iconName, onPress } = this.props;
    return(
     <TouchableOpacity onPress={onPress}>
        <View style={styles.iconContainer}>
          <Feather name={iconName} size={23}/>
        </View>
      </TouchableOpacity>
    );
  }
}

HeaderIconButton.propTypes = {
  iconName: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
}

const styles = StyleSheet.create({
  iconContainer: {
    paddingLeft: 20,
    paddingRight: 20
  }
});