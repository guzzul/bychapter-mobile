import React from 'react';
import { Text, View, StyleSheet } from 'react-native';

const avatarSize = 36;
const avatarContainerPadding = 0;

export class ChapterAvatar extends React.PureComponent {
  render() {
    const { label } = this.props;
    return(
      <View style={styles.container}>
        <View style={styles.textWrapper}>
          <Text style={styles.text}>{label}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'flex-end',
    paddingTop: avatarContainerPadding,
  },
  textWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    width: avatarSize,
    height: avatarSize,
    backgroundColor: '#3399FF',
  },
  text: {
    color: 'white',
    fontSize: 16,
    fontFamily: 'montserrat-bold'
  }

});