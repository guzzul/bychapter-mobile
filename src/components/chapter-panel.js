import React from 'react';
import { 
  Dimensions, 
  StyleSheet, 
  Text, 
  TouchableOpacity, 
  View 
} from 'react-native';

import Collapsible from 'react-native-collapsible';
import Swipeable from 'react-native-swipeable-row';

import { ChapterAvatar } from './chapter-avatar';

const { width } = Dimensions.get('window')

export class ChapterPanel extends React.Component {

  constructor(props) {
    super(props);
    const { isRead } = this.props;
    this.state = { isCollapsed: isRead };
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.isRead !== nextProps.isRead) {
      return true;
    }
    if (this.state.isCollapsed !== nextState.isCollapsed) {
      return true;
    }
    return false;
  }

  render() {
    const { isCollapsed } = this.state;
    const { chapter, title, onSwipeComplete, bounce } = this.props;
    return (
      <Collapsible collapsed={isCollapsed}>
        <Swipeable
          bounceOnMount={bounce}
          leftButtonWidth={width}
          leftButtons={[
            <TouchableOpacity style={[styles.leftSwipeItem, {backgroundColor: 'yellowgreen'}]} />
          ]}
          onLeftButtonsOpenComplete={() => {
            this.setState({isCollapsed: true});
            onSwipeComplete(chapter);
          }}
        >

          <View style={[styles.listItem, { backgroundColor: 'white' }]}>
            <ChapterAvatar label={chapter}/>
            <View style={{width: 15}}></View>
            <Text style={ styles.text }>{title}</Text>
          </View>

        </Swipeable>
      </Collapsible>
    );
  }
}

const styles = StyleSheet.create({
  listItem: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    paddingLeft: 20,
    paddingRight: 20,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#bbb',
  },
  leftSwipeItem: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: 20,
  },
  rightSwipeItem: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 20
  },
  text: {
    flex: 1, 
    flexWrap: 'wrap',
    fontFamily: 'raleway-regular',
    fontSize: 16,
  }
});
