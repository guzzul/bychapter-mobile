import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export class ProgressBar extends React.Component {
  render() {
    const { percent, label } = this.props;
    return(
      <View style={styles.container}>
        <View style={styles.outer}>
          <View style={[styles.inner, {width: `${percent}%`} ]} />
          <Text style={styles.text}>{label}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  outer: {
    backgroundColor: "#fff",
    marginTop: 20,
    marginHorizontal: 10,
    flex: 1,
    width: '100%',
    height: 40,
    borderColor: 'white',
    borderWidth: 2,
    borderRadius: 30,
    padding: 3,
    justifyContent: 'center'
  },
  inner: {
    width: '0%',
    height: 30,
    borderRadius: 50,
    backgroundColor: 'yellowgreen',
  },
  text: {
    fontFamily: 'montserrat-regular',
    fontWeight: 'bold',
    fontSize: 20,
    color: "#4A4A4A",
    position: 'absolute',
    zIndex: 1,
    alignSelf: 'flex-end',
    paddingRight: 10
  }
})
