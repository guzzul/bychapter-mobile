import React from 'react';

import { StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native';
import PropTypes from 'prop-types';

import ProgressCircle from 'react-native-progress-circle';

export class BookPanel extends React.Component {
  static propTypes = {
    label: PropTypes.string.isRequired,
    book: PropTypes.string.isRequired,
    progress: PropTypes.number.isRequired,
    onPress: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props);
  }

  render() {
    const { label, book, progress, onPress } = this.props;
    return(
      <TouchableWithoutFeedback
        onPress={() => onPress(book)}
        >

        <View style={ styles.listItem }>
          <ProgressCircle
            percent={progress}
            radius={19}
            borderWidth={4}
            color="#3399FF"
            shadowColor="#999"
            bgColor="#fff"
          >
            <Text style={{ fontSize: 10, fontFamily: 'montserrat-medium' }}>{progress + '%'}</Text>
          </ProgressCircle>
          <View style={{width: 15}}></View>
          <Text style={ styles.text }>{ label }</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  listItem: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: 'white',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#bbb',
  },
  text: {
    fontFamily: 'montserrat-regular',
    fontSize: 18
  }


});
