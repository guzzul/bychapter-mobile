import React, { Component } from 'react';
import { 
  Alert,
  AsyncStorage, 
  FlatList, 
  Platform, 
  StyleSheet, 
  Text, 
  SafeAreaView, 
  View 
} from 'react-native';

import { connect } from 'react-redux';
import { Constants } from 'expo';
import { Feather } from '@expo/vector-icons';

import { distanceInWordsToNow, parse } from 'date-fns';

import { HeaderIconButton } from '../components/header-icon-button';
import { UndoPanel } from '../components/undo-panel';
import { AdmobPanel } from '../components/admob-panel';

// Redux Actions
import { markChapterAsUnread, clearCompletedChapters } from '../actions/main-action';

const ITEM_HEIGHT = 50;
const getItemLayout = (data, index) => (
  { length: ITEM_HEIGHT, offset: ITEM_HEIGHT * index, index }
);

class UndoScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      count: 0,
      undoList: this.props.readList,
    }
  }

  // Save props to storage
  async _updateStorage() {
    try {
      await AsyncStorage.setItem('book_stats', JSON.stringify(this.props.bookStats));
      await AsyncStorage.setItem('book_read_list', JSON.stringify(this.props.readList));
      await AsyncStorage.setItem('summary_stats', JSON.stringify(this.props.summaryStats));
    } catch(error) {
      console.log("Error reading App Info:" + error);
    }
  }

  // Clear storage
  async _clearStorage() {
    if (__DEV__) {
      console.log('Clearing async storage.')
    }
    try {
      //await AsyncStorage.clear();
      await AsyncStorage.setItem('book_stats', JSON.stringify(this.props.bookStats));
      await AsyncStorage.setItem('book_read_list', JSON.stringify(this.props.readList));
      await AsyncStorage.setItem('summary_stats', JSON.stringify(this.props.summaryStats));
    } catch(error) {
      console.log("Error clearing asynch storage. " + error);
    }
  }

  _onMarkedUnread = (book, chapter) => {
    this.props.markChapterAsUnread({ book: book, chapter: chapter });
    this._updateStorage();
    this.state = {
      count: this.state.count + 1,
      undoList: this.props.readList,
    }
  }

  _verifyClearCompletedChapters = () => {
    Alert.alert(
      'Clear Completed Chapters',
      'You are about to clear all completed chapters. Do you want to continue?',
      [
        {text: 'Cancel', onPress: () => {}, style: 'cancel'},
        {text: 'OK', onPress: () => { this._clearCompletedChapters() } },
      ],
      { cancelable: false }
    )
  }

  _clearCompletedChapters = () => {
    this.props.clearCompletedChapters();
    this._clearStorage();

    this.setState({
      count: this.state.count + 1,
      undoList: this.props.readList,
    })
  }

  _renderUndoHeader = () => {
    const { navigation } = this.props;
    return(
      <View style={styles.header}>
        <HeaderIconButton iconName="x" onPress={ ()=>  navigation.goBack() } />
        <Text style={{ fontFamily: 'montserrat-bold', fontSize: 18 }}>Completed Chapters</Text>
        <HeaderIconButton iconName="trash-2" onPress={ ()=>  this._verifyClearCompletedChapters() } />
      </View>
    );
  }

  _renderListHeader = () => {
    return(
      <AdmobPanel />
    );
  }

  _renderListItem = ({item}) => {
    const age = distanceInWordsToNow(parse(item.date));
    return(
      <UndoPanel 
        book={ item.book }
        chapter={ item.chapter }
        age={ age }
        isRead={true}
        onMarkedUnread={ this._onMarkedUnread }
      />
    );
  }
  _renderListFooter = () => {
    return (
      <Feather style={{ alignSelf: 'center', paddingTop: 10, paddingBottom: 10, color: 'darkgrey'}} name='more-horizontal' size={20}/>
    )
  }

  render() {
    const showUndoList = this.state.undoList.length > 0;
    const showNoUndo = this.state.undoList.length <= 0;
    return(
      <SafeAreaView style={styles.container}> 
        <View style={{ height: Platform.OS === 'ios' ? 0 : Constants.statusBarHeight }}/>

        { this._renderUndoHeader() }

        <View style={{ flex: 1 }}>
        { showUndoList &&
        
        <FlatList
            data = { this.state.undoList }
            renderItem = { this._renderListItem }
            ListHeaderComponent = { this._renderListHeader }
            ListFooterComponent = { this._renderListFooter }
            keyExtractor = {(item, index) => index.toString() }
            extraData = { this.state.count }
            // Performance
            initialNumToRender={12}
            getItemLayout={getItemLayout}
            style={{ backgroundColor: 'gainsboro' }}
          />
        }

        { showNoUndo &&
          <View style={{  alignContent: 'center', justifyContent: 'center', height: 200 }}>
            <Feather style={{ alignSelf: 'center' }} name='hash' size={40}/>
            <Text style={{ alignSelf: 'center', fontSize: 14, paddingTop: 20 }}>No completed chapters yet.</Text>
          </View>
        }

        </View>

      </SafeAreaView>
    );
  }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  header: {
    height: (Platform.OS === 'ios' ? 44 : 56),
    backgroundColor: 'white',
    borderBottomColor: '#bbb',
    borderBottomWidth: StyleSheet.hairlineWidth,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    color: 'blue',
    fontWeight: 'bold',
  },
  subtitle: {
    fontSize: 16,
    color: 'purple',
    fontWeight: 'bold',
  },
});

// Redux mappings
const mapStateToProps = (state) => {
  return {
    bookStats: state.main.bookStats,
    readList: state.main.readList,
    summaryStats: state.main.summaryStats
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    markChapterAsUnread: (data) => dispatch(markChapterAsUnread(data)),
    clearCompletedChapters: () => dispatch(clearCompletedChapters()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UndoScreen);
