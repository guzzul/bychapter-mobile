import React, { Component } from 'react';
import { AsyncStorage, FlatList, SafeAreaView, View ,Text, TouchableHighlight} from 'react-native';

import { NavigationEvents } from 'react-navigation';
import { Feather } from '@expo/vector-icons';

import { connect } from 'react-redux';

import { BibleBooksTitleMap, BibleBooksChapterMap, ChapterTitleMap } from '../const';
import { ChapterPanel } from '../components/chapter-panel';
import { HeaderIconButton } from '../components/header-icon-button';
import { AdmobPanel } from '../components/admob-panel';

// Redux Actions
import { markChapterAsRead, markChapterAsUnread } from '../actions/main-action';

const ITEM_HEIGHT = 50;
const getItemLayout = (data, index) => (
  { length: ITEM_HEIGHT, offset: ITEM_HEIGHT * index, index }
);

class DetailScreen extends Component {

  chapterList = [];

  static navigationOptions = ({ navigation }) => {
    return {
      title: BibleBooksTitleMap[navigation.getParam('book')],
      headerTitleStyle: {
        fontFamily: 'montserrat-bold',
        fontSize: 18
      },
      swipeEnabled: true,
      headerLeft: (
        <HeaderIconButton iconName="arrow-left" onPress={ ()=>  navigation.goBack() }/>
      ),
      headerRight: (
        <HeaderIconButton iconName="check-square" onPress={ ()=>  navigation.navigate('Undo') }/>
      ),
    };
  };

  constructor(props) {
    super(props);

    this.state = {
      count: 0,
      showList: false,
      chapterList: [],
    }
  }

  _getChapterTitle = (chapter) => {
    const { navigation } = this.props;
    const book = navigation.getParam('book');

    return ChapterTitleMap[book][chapter];
  }

 
  _initChapterList() {
    const book = this.props.navigation.getParam('book');
    const bookChapters = BibleBooksChapterMap[book];
    const readChapters = this.props.bookStats[book].chaptersReadList;

    const tmpChapterList = [];
    bookChapters.forEach(chapter => {
      if(!readChapters.includes(chapter)) {
        tmpChapterList.push(chapter);
      }
    });

    const newChapterList = { chapterList: [...tmpChapterList] };
    this.setState( { ...newChapterList } );
  }

  _renderListHeader = () => {
    return(
      <View style={{ flex: 1, backgroundColor: '#f8f8f8', }}>
        <AdmobPanel />
        <View style={{ paddingTop: 5, paddingBottom: 5, flexDirection: 'row', alignItems: 'center',  justifyContent: 'center' }} >
          <Feather name="info" size={16} color='grey'/>
          <Text style={{ fontFamily: 'raleway-regular', fontSize: 12, paddingLeft: 5 }}>
            Swipe right to mark a chapter as read.
          </Text>
        </View>
      </View>
      
    );
  }

  _renderListFooter = () => {
    return (
      <Feather style={{ alignSelf: 'center', paddingTop: 10, paddingBottom: 10, color: 'darkgrey'}} name='more-horizontal' size={20}/>
    )
  }

  _renderListItem = ({item}) => {
    const book = this.props.navigation.getParam('book');
    const chapters = this.props.bookStats[book].chaptersReadList;

    return(
      <ChapterPanel
        chapter={ item }
        title={ this._getChapterTitle(item) }
        isRead = { chapters.includes(item) }
        bounce = { item === 1 }
        onSwipeComplete={ this._onSwipeComplete }
        />
    );
  }

  _onSwipeComplete = chapter => {
    // Mark current chapter as read
    const book = this.props.navigation.getParam('book');
    this.props.markChapterAsRead( {book: book, chapter: chapter, date: new Date() });

    this._updateStorage();
  }

  // Save BookStats to storage
  async _updateStorage() {
    try {
      await AsyncStorage.setItem('book_stats', JSON.stringify(this.props.bookStats));
      await AsyncStorage.setItem('book_read_list', JSON.stringify(this.props.readList));
      await AsyncStorage.setItem('summary_stats', JSON.stringify(this.props.summaryStats));
    } catch(error) {
      console.log("Error reading App Info:" + error);
    }
  }

  render() {
    const book = this.props.navigation.getParam('book');
    const chapters = [...BibleBooksChapterMap[book]];
    return(
      <SafeAreaView style={{ flex: 1 }}>
        <NavigationEvents
          onWillFocus={ () => {
            this.setState( { showList: false } );
            this._initChapterList();
          }}
          onDidFocus= { () => {
            this.setState( { count: this.state.count + 1} );
            this.setState( { showList: true } );
          }}
        />
        
        <View style={{ flex: 1 }}>
        { this.state.showList &&
        <FlatList
            data = { this.state.chapterList }
            renderItem = { this._renderListItem }
            keyExtractor = {(item, index) =>  index.toString() }
            ListHeaderComponent = { this._renderListHeader }
            ListFooterComponent = { this._renderListFooter }
            extraData = { this.state.count }
            // Performance
            initialNumToRender={15}
            getItemLayout={getItemLayout}
            removeClippedSubviews={true}
          />
        }  

        </View>
        
      </SafeAreaView>
    );
  }
}


// Redux mappings
const mapStateToProps = (state) => {
  return {
    bookStats: state.main.bookStats,
    readList: state.main.readList,
    summaryStats: state.main.summaryStats
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    markChapterAsRead: (data) => dispatch(markChapterAsRead(data)),
    markChapterAsUnread: (data) => dispatch(markChapterAsUnread(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailScreen);
