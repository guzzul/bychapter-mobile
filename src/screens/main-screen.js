import React, { Component } from 'react';
import { 
  ActivityIndicator, 
  AsyncStorage, 
  Dimensions, 
  FlatList,
  SafeAreaView, 
  ScrollView,
  StyleSheet, 
  Text, 
  View } 
from 'react-native';

import { NavigationEvents } from 'react-navigation';
import { connect } from 'react-redux';

import { Feather } from '@expo/vector-icons';

import SwiperFlatList from 'react-native-swiper-flatlist';
import ProgressCircle from 'react-native-progress-circle';

import { HeaderIconButton } from '../components/header-icon-button';
import { ProgressBar } from '../components/progress-bar';

import { 
  OldTestamentBooks, 
  NewTestamentBooks, 
  BibleBooksTitleMap, 
  TOTAL_CHAPTERS,
  OLD_TESTAMENT_CHAPTERS,
  NEW_TESTAMENT_CHAPTERS,
  getInitialSummaryStats,
  getInitialBookStats,
} from '../const';

import { BookPanel } from '../components/book-panel';

import { initAsyncStore, initAsyncStoreSucceeded } from '../actions/main-action';

const { width, height } = Dimensions.get('window');

const APP_INFO = {
  app_name: 'ByChapter',
  app_version: '1.0'
};

class MainScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'By Chapter',
      headerTitleStyle: {
        fontFamily: 'montserrat-bold',
        fontSize: 18
      },
      swipeEnabled: true,
      headerRight: (
        <HeaderIconButton iconName="check-square" onPress={ ()=>  navigation.navigate('Undo') }/>
      ),
    }
  };

  constructor(props) {
    super(props);

    this.state = {
      count: 0,
      storageInitialized: false,
      overallProgress: 0,
    }
  }


  componentDidMount(){
    this._initStorage();
  }

  async _initStorage() {
    if ( !this.state.storageInitialized) {
      this.props.initAsyncStore();
      let initOverallProgress = 0;
      try {
        const appInfoStr = await AsyncStorage.getItem('app_info');
        const bookStatsStr = await AsyncStorage.getItem('book_stats');
        const bookReadListStr = await AsyncStorage.getItem('book_read_list');
        const summaryStatsStr = await AsyncStorage.getItem('summary_stats');

        if (appInfoStr === null) {
          await AsyncStorage.setItem('app_info', JSON.stringify(APP_INFO));
          await AsyncStorage.setItem('book_stats', JSON.stringify(getInitialBookStats()));
          await AsyncStorage.setItem('book_read_list', JSON.stringify([]));
          await AsyncStorage.setItem('summary_stats', JSON.stringify(getInitialSummaryStats()));

          const bookStatsStr = await AsyncStorage.getItem('book_stats');
          const bookStats = JSON.parse(bookStatsStr);

          const bookReadListStr = await AsyncStorage.getItem('book_read_list')
          const bookReadList = JSON.parse(bookReadListStr);

          const summaryStatsStr = await AsyncStorage.getItem('summary_stats');
          const summaryStats = JSON.parse(summaryStatsStr);

          this.props.initAsyncStoreSucceeded({ 
            bookStats: bookStats, 
            bookReadList: bookReadList,
            summaryStats: summaryStats
           });

           initOverallProgress = summaryStats.overallProgress;
        } else {
          const bookStats = JSON.parse(bookStatsStr);
          const bookReadList = JSON.parse(bookReadListStr);
          const summaryStats = JSON.parse(summaryStatsStr);

          this.props.initAsyncStoreSucceeded({ 
            bookStats: bookStats, 
            bookReadList: bookReadList,
            summaryStats: summaryStats
           });
           initOverallProgress = summaryStats.overallProgress;
        }

        this.setState({
          storageInitialized: true,
          overallProgress: initOverallProgress
        });

      } catch(error) {
        console.log("Error reading App Info:" + error);
      }
    }
  }

  // Flatlist helper functions
  _renderListHeader = () => {
    return(
      <View style={{ height: 34 }}></View>
    );
  }

  _renderOldTestamentListHeader = () => {
    return(
      <View style={{ height: 40, backgroundColor: '#3399FF', justifyContent: 'center' }}>
        <Text style={{ fontFamily: 'montserrat-bold', color: 'white', fontSize: 20, paddingLeft: 20}}>Old Testament</Text>
      </View>
    )
  }

  _renderNewTestamentListHeader = () => {
    return(
      <View style={{ height: 40, backgroundColor: '#3399FF', justifyContent: 'center' }}>
        <Text style={{ fontFamily: 'montserrat-bold', color: 'white', fontSize: 20, paddingLeft: 20 }}>New Testament</Text>
      </View>
    )
  }

  _renderListItem = ({item}) => {
    return(
      <BookPanel
        label={ BibleBooksTitleMap[item] }
        book={ item }
        progress={ this.props.bookStats[item].percentComplete }
        onPress={ this._onBookPanelPress }/>
    );
  }

  // Event callbacks
  _onBookPanelPress = book => {
    const { navigation } = this.props;
    navigation.navigate('Detail', { book: book });
  }


  // Main render function
  render() {

    if (!this.state.storageInitialized)
      return(
        <View style={{ flex: 1, alignContent: 'center', justifyContent: 'center'}}>
          <ActivityIndicator size = "large" style={{ height: 80 }}/>
        </View>
      );
      
      const chaptersRemaining = String(TOTAL_CHAPTERS - this.props.summaryStats.chaptersRead);
      const ntStats = String(this.props.summaryStats.ntChaptersRead) + '/' + String(NEW_TESTAMENT_CHAPTERS);
      const otStats = String(this.props.summaryStats.otChaptersRead) + '/' + String(OLD_TESTAMENT_CHAPTERS);

      return (
        <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
        <SwiperFlatList 
          showPagination={true}
          paginationDefaultColor='gray'
          paginationActiveColor='black'
          paginationStyleItem={{
            width: 8,
            height: 8,
            borderRadius: 4,
            marginLeft: 3,
            marginRight: 3,
            marginTop: 3,
            marginBottom: 3}}
        >

          <View style={styles.chartContainer}>
          <ScrollView>
            <NavigationEvents
              onWillFocus={ () => {
                // Increment state counter to force flatlist to refresh
                this.setState({ 
                  count: this.state.count + 1,
                  overallProgress: this.props.summaryStats.overallProgress
                 });
              }}
            />

            <View style={styles.chartContainerFirst}>
              <View style={{ height: 10 }}></View>
              <ProgressCircle
                percent={ this.state.overallProgress }
                radius={ (width/2) - 70}
                borderWidth={20}
                color="#3399FF"
                shadowColor="#999"
                bgColor="#fff"
              >
                <Text style={{ color: "#4A4A4A", fontFamily: 'montserrat-bold', fontSize: 40, fontWeight: 'bold' }}>{this.state.overallProgress + '%'}</Text>
              </ProgressCircle>
              <Text style={{ color: "#4A4A4A", paddingTop: 10, paddingBottom: 10, fontFamily: 'montserrat-bold', fontSize: 20 }}>Overall Progress</Text>

            </View>

            <View style={styles.chartContainerSecond}>
              <View style={{ width: width-20, height: 30, justifyContent:'flex-end'}}>
                <Text style={{color: 'white', left: 10, top: 5, fontSize: 18, fontFamily: 'montserrat-bold' }}>Chapters Remaining</Text>
              </View>

              <View style={{ width: width-20, height: 58 }}>
                <ProgressBar percent={this.state.overallProgress} label={chaptersRemaining}/>
              </View>
              
              <View style={{ width: width-20, height: 30, justifyContent:'flex-end'}}>
                <Text style={{color: 'white', left: 10, top: 5, fontSize: 18, fontFamily: 'montserrat-regular' }}>Old Testament</Text>
              </View>

              <View style={{ width: width-20, height: 58 }}>
                <ProgressBar percent={this.props.summaryStats.otProgress} label={otStats}/>
              </View>
              
              <View style={{ width: width-20, height: 30, justifyContent:'flex-end'}}>
                <Text style={{color: 'white', left: 10, top: 5, fontSize: 18, fontFamily: 'montserrat-regular' }}>New Testament</Text>
              </View>

              <View style={{ width: width-20, height: 58 }}>
                <ProgressBar percent={this.props.summaryStats.ntProgress} label={ntStats}/>
              </View>

            </View>
          </ScrollView>  
          </View>

          <View  style={styles.slide}>
            <FlatList
              data = { OldTestamentBooks }
              renderItem = { this._renderListItem }
              keyExtractor = { (item, index) => index.toString() }
              ListHeaderComponent = { this._renderOldTestamentListHeader }
              extraData = { this.state.count }
              style={{ backgroundColor: 'gainsboro' }}
            />
          </View>

          <View style={styles.slide}>
            <FlatList
              data = { NewTestamentBooks }
              renderItem = { this._renderListItem }
              keyExtractor = { (item, index) => index.toString() }
              ListHeaderComponent = { this._renderNewTestamentListHeader }
              extraData = { this.state.count }
              style={{ backgroundColor: 'gainsboro' }}
            />
          </View>
         
        </SwiperFlatList>
         
        </SafeAreaView>
      );


  }
}

const styles = StyleSheet.create({
  wrapper: {
  },
  chartContainer: {
    flex: 1,
  },
  chartContainerFirst: {
    flex: 1,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor: 'white',
    paddingTop: 40,
    width: width,
    //height: (height / 3) 
  },
  chartContainerSecond: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#3399FF',
    paddingTop: 10,
    paddingBottom: 120
  },
  slide: {
    flex: 1,
    width: width,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
});


/* Redux Map To Props */

const mapStateToProps = (state) => {
  return {
    bookStats: state.main.bookStats,
    readList: state.main.readList,
    summaryStats: state.main.summaryStats
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    initAsyncStore: () => dispatch(initAsyncStore()),
    initAsyncStoreSucceeded: (data) => dispatch(initAsyncStoreSucceeded(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);
