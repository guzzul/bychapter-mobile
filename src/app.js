import React, { Component } from 'react';

import { combineReducers, createStore, applyMiddleware } from 'redux'
import { Provider, connect } from 'react-redux';
import thunk from 'redux-thunk';

import { Font } from 'expo';
import { mainReducer } from './reducers/main-reducer';
import { RootNavigator } from './navigators/root-navigator';

const rootReducer = combineReducers({ main: mainReducer });
export const store = createStore(rootReducer, applyMiddleware(thunk));

export default class ByChapterApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fontsLoaded: false,
    }
  }
  async componentDidMount() {
    this.setState({ fontsLoaded: false })
    
    await Font.loadAsync({
      'montserrat-regular': require('../assets/fonts/Montserrat-Regular.ttf'),
      'montserrat-medium': require('../assets/fonts/Montserrat-Medium.ttf'),
      'montserrat-bold': require('../assets/fonts/Montserrat-Bold.ttf'),
      'raleway-regular': require('../assets/fonts/Raleway-Regular.ttf'),
    });
    
    this.setState({ fontsLoaded: true })
  }

  render() {
    if (!this.state.fontsLoaded) return null;

    return (
      <Provider store={store}>
        <RootNavigator />
      </Provider>
    );
  }
}
