import { Platform } from 'react-native';

import { createStackNavigator } from 'react-navigation';
import MainScreen from '../screens/main-screen';
import DetailScreen from '../screens/detail-screen';
import UndoScreen from '../screens/undo-screen';

const MainNavigator = createStackNavigator(
  {
    Main: MainScreen,
    Detail: DetailScreen,
  },
  {
    initialRouteName: 'Main',
    navigationOptions: {
      headerTitleStyle: {
        fontWeight: Platform.OS === 'ios' ? 'bold' : 'normal'
      },
    }
  }
);

export const RootNavigator = createStackNavigator(
  {
    Main: MainNavigator,
    Undo: UndoScreen,
  },
  {
    mode: 'modal',
    headerMode: 'none',
  }
)
