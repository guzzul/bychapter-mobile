
import { 
  INIT_ASYNCSTORE, 
  INIT_ASYNCSTORE_SUCCEEDED,
  INIT_ASYNCSTORE_FAILED,
  MARK_CHAPTER_AS_READ,
  MARK_CHAPTER_AS_UNREAD,
  CLEAR_COMPLETED_CHAPTERS,

  BibleBooksChapterMap,
  NewTestamentBooks,

  TOTAL_CHAPTERS,
  OLD_TESTAMENT_CHAPTERS,
  NEW_TESTAMENT_CHAPTERS,

  getInitialSummaryStats,
  getInitialBookStats
} from '../const';

const initialState = {
  bookStats: getInitialBookStats() ,
  readList: [],
  summaryStats: getInitialSummaryStats() 
};

export function mainReducer(state = initialState, action) {
  switch(action.type) {

    case INIT_ASYNCSTORE: {
      return {
        ...state,
        bookStats: {},
        readList: [],
        summaryStats: {}
      }
    }

    case INIT_ASYNCSTORE_SUCCEEDED: {
      return {
        ...state,
        bookStats: action.data.bookStats,
        readList: action.data.bookReadList,
        summaryStats: action.data.summaryStats
      }
    }

    case MARK_CHAPTER_AS_READ: {
      const { book } = action.data;
      const newBookStats = { ...state.bookStats };
      const newReadList = [ ...state.readList ];
      const newSummaryStats = { ...state.summaryStats };

      // Update book stats
      newBookStats[book].chaptersReadList.unshift(action.data.chapter);
      newBookStats[book].chaptersRead++;
      newBookStats[book].percentComplete = Math.round((newBookStats[book].chaptersRead / BibleBooksChapterMap[action.data.book].length) * 100);

      // Update read list
      newReadList.unshift({book: action.data.book, chapter: action.data.chapter, date: action.data.date});

      // Update summary stats
      newSummaryStats.chaptersRead++;
      const progress = (newSummaryStats.chaptersRead / TOTAL_CHAPTERS) * 100;
      newSummaryStats.overallProgress = Number(progress.toFixed(2));

      // If New Testament
      if (NewTestamentBooks.includes(action.data.book)) {
        newSummaryStats.ntChaptersRead++;
        const ntProgress = (newSummaryStats.ntChaptersRead / NEW_TESTAMENT_CHAPTERS) * 100;
        newSummaryStats.ntProgress = Number(ntProgress.toFixed(2));
      } else {
        newSummaryStats.otChaptersRead++;
        const otProgress = (newSummaryStats.otChaptersRead / OLD_TESTAMENT_CHAPTERS) * 100;
        newSummaryStats.otProgress = Number(otProgress.toFixed(2));
      }

      return {
        bookStats: newBookStats,
        readList: newReadList,
        summaryStats: newSummaryStats
      };
    }

    case MARK_CHAPTER_AS_UNREAD: {
      const { book } = action.data;
      const newBookStats = { ...state.bookStats };
      const newReadList = [ ...state.readList ];
      const newSummaryStats = { ...state.summaryStats };

      // Update book stats
      const newChapterReadList = newBookStats[book].chaptersReadList.filter(item => item != action.data.chapter);
      newBookStats[book].chaptersReadList = [...newChapterReadList];
      newBookStats[book].chaptersRead--;
      newBookStats[book].percentComplete = Math.round((newBookStats[book].chaptersRead / BibleBooksChapterMap[action.data.book].length) * 100);

      // Update read list
      const tmpReadList = [];
      for(var i=0; i<newReadList.length; i++) {
        if(newReadList[i].book === action.data.book && newReadList[i].chapter === action.data.chapter) {
          // ES6 Bug: !== does not works
        } else {
          tmpReadList.push(newReadList[i]);
        }
      }

      newReadList = [...tmpReadList];

      // Update summary stats
      newSummaryStats.chaptersRead--;
      const progress = (newSummaryStats.chaptersRead / TOTAL_CHAPTERS) * 100;
      newSummaryStats.overallProgress = Number(progress.toFixed(2));

      // If New Testament
      if (NewTestamentBooks.includes(action.data.book)) {
        newSummaryStats.ntChaptersRead--;
        const ntProgress = (newSummaryStats.ntChaptersRead / NEW_TESTAMENT_CHAPTERS) * 100;
        newSummaryStats.ntProgress = Number(ntProgress.toFixed(2));
      } else {
        newSummaryStats.otChaptersRead--;
        const otProgress = (newSummaryStats.otChaptersRead / OLD_TESTAMENT_CHAPTERS) * 100;
        newSummaryStats.otProgress = Number(otProgress.toFixed(2));
      }

      return {
        bookStats: newBookStats,
        readList: newReadList,
        summaryStats: newSummaryStats
      };

    }

    case CLEAR_COMPLETED_CHAPTERS: {
      return {
        bookStats: getInitialBookStats() ,
        readList: [],
        summaryStats: getInitialSummaryStats()
      }
    }

    default: return state;
  }
}
