import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';

import { 
  INIT_ASYNCSTORE, 
  INIT_ASYNCSTORE_SUCCEEDED,
  INIT_ASYNCSTORE_FAILED,
  MARK_CHAPTER_AS_READ,
  MARK_CHAPTER_AS_UNREAD,
  CLEAR_COMPLETED_CHAPTERS
} from '../const';


export function initAsyncStore() {
  return {
    type: INIT_ASYNCSTORE
  }
}

export function initAsyncStoreSucceeded(data) {
  return {
    type: INIT_ASYNCSTORE_SUCCEEDED,
    data
  }
}

export function markChapterAsRead(data) {
  return {
    type: MARK_CHAPTER_AS_READ,
    data
  }
}

export function markChapterAsUnread(data) {
  return {
    type: MARK_CHAPTER_AS_UNREAD,
    data
  }
}

export function clearCompletedChapters() {
  return {
    type: CLEAR_COMPLETED_CHAPTERS,
  }
}