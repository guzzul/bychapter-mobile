import React from 'react';
import ByChapterApp from './src/app';

export default class App extends React.Component {
  render() {
    return (
      <ByChapterApp />
    );
  }
}

